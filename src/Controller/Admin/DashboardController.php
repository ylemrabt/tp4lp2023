<?php

namespace App\Controller\Admin;

use App\Entity\Artiste;
use App\Entity\Concert;
use App\Entity\Email;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(ConcertCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('default/dashboard.html.twig');
    }

    public function configureActions(): Actions
    {
        $actions = parent::configureActions();
        return $actions
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pen-to-square')->setLabel(false);
            });
    }

    public function configureCrud(): Crud
    {
        $crud = parent::configureCrud();
        return $crud
            ->showEntityActionsInlined();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration de la Sirène')
            ->renderContentMaximized(true);
    }

    public function configureMenuItems(): iterable
    {
        return [
            MenuItem::section('Admin', 'fa-solid fa-gears'),
            MenuItem::linkToCrud('Les concerts', 'fa-solid fa-volume-high', Concert::class)
                ->setController(ConcertCrudController::class),
            MenuItem::linkToCrud('Les concerts passés', 'fa-solid fa-volume-high', Concert::class)
                ->setController(ConcertPassesCrudController::class),
            MenuItem::linkToCrud('Les artists', 'fa-solid fa-user', Artiste::class)
                ->setController(ArtisteCrudController::class),
            MenuItem::linkToCrud('Newsletter', 'fa-solid fa-paper-plane', Email::class)
                ->setController(EmailCrudController::class)
        ];
    }
}
